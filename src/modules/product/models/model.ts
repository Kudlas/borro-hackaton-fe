export interface NP_step1Data {
    title: undefined,
    area: undefined,
    category: undefined,
    payment: undefined,
    paymentWanted: true,
    rewardAmount: undefined,
}