import React from 'react';
import 'firebase/auth';
import './App.css';
import Loading from "./modules/loading/components/loading";
import AppNavbar from "./modules/layout/components/appNavbar";
import Routes from "./modules/router/routes";
import NewProductModal from "./modules/product/components/newProduct/NewProductModal";

function App() {
    return (
        <div className="App">
            <AppNavbar isLoggedIn={true}/>
            <Loading/>

            <NewProductModal />
            <Routes/>
        </div>
    );
}

export default App;
