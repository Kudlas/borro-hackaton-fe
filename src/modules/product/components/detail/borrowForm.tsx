import React, {useState} from "react";

// import {MuiPickersUtilsProvider} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import {differenceInDays} from 'date-fns'
import csLocale from "date-fns/locale/cs";

// import {InlineDatePicker} from 'material-ui-pickers';
import {isSameDay} from "date-fns";

// import DateRangePicker from "./DateRangePicker";
import {Divider, Typography} from "@material-ui/core";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const getDayPostfix = (count) => {
    let ret = 'den';
    if (count > 1) ret = 'dny';
    if (count > 4) ret = 'dní';

    return ret;
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        list: {
            '& li':{
                display: 'flex',
                justifyContent: 'space-between'
            },
            color: '#000',
            padding: 0,
            listStyle: 'none',
            marginBottom: theme.spacing(1)
        },
        picker: {
            color: 'white'
        },
        head: {
            color: '#282c34',
            marginBottom: theme.spacing(2)
        },
        total: {
            display: 'block',
            margin: theme.spacing(1)
        }
    }));

const BorrowForm = ({price}) => {

    const classes = useStyles();

    const [days, setDays] = useState(0);

    const calculation = (<div>
        <ul className={classes.list}>
            <li>Počet dní: <div>{days} {getDayPostfix(days)}</div></li>
            <li>{price} Kč x {days} {getDayPostfix(days)} <div>{price * days} Kč</div> </li>
        </ul>
        <Divider/>
        <strong className={classes.total}>Celkem {price * days} Kč</strong>
    </div>);

    return (
        <div></div>
        /*<MuiPickersUtilsProvider utils={DateFnsUtils} locale={csLocale}>
            <Typography variant="h4" component="h2" className={classes.head}> {price} Kč <span>/ den</span></Typography>
            <div className={classes.head}>
                <DateRangePicker
                    variant='outlined'
                    onlyCalendar
                    format={'d.M.y'}
                    helperText="Na kdy to potřebuješ?"
                    pickerComponent={InlineDatePicker}
                    shouldDisableDate={day => isSameDay(day, new Date("2020-08-20"))}
                    minDate={new Date()}
                    value={[new Date("2020-08-18"), new Date("2020-08-18")]}
                    onChange={(d1, d2) => {
                        setDays(Math.abs(differenceInDays(d1, d2)) + 1)
                    }}
                />
            </div>

            {((days !== 0) && calculation)}

        </MuiPickersUtilsProvider>*/
    );
};

export default BorrowForm;
