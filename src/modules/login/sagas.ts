import {call, put, takeLatest} from 'redux-saga/effects'

import {
    LoginActionTypes,
    LoginFail,
    LoginSuccess,
} from "./actions";
import {LoginService} from "./services";
import app from "firebase";
import firebase from "firebase";

function* postLogin(action) {

    try {
         const data = yield call(LoginService.postLogin, action.creds );
         yield put(LoginSuccess(data));
    } catch(e) {
        yield put( LoginFail(e) );
    }
}

function* LoginSaga() {
    yield takeLatest(LoginActionTypes.Login, postLogin);
    yield takeLatest(LoginActionTypes.FacebookLogin, () => firebase.auth().signInWithPopup( new app.auth.FacebookAuthProvider() ) );
    yield takeLatest(LoginActionTypes.GoogleLogin, () => firebase.auth().signInWithPopup( new app.auth.GoogleAuthProvider() ) );
}

export default LoginSaga;