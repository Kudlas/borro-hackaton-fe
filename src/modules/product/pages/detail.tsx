import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getid} from "../../router/selectors";
import {GetProductDetail} from "../actions/detail";
import {getDetail} from "../selectors";
import {Box, Button, Container, Divider, Grid, Typography} from "@material-ui/core";
import ShareIcon from '@material-ui/icons/Share';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ImagesGrid from "../components/detail/ImagesGrid";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import UserDisplay from "../../layout/components/userDisplay";
import BorrowForm from "../components/detail/borrowForm";
import Carousel, {Modal, ModalGateway} from "react-images";
import {useFirestore, useFirestoreConnect} from "react-redux-firebase";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            color: 'white'
        },
        box: {
            backgroundColor: '#464D59',
            align: 'left',
            display:'flex',
            flexDirection: "column",
            textAlign: 'left',
            padding: theme.spacing(4)
        },
        small: {
            width: theme.spacing(5),
            height: theme.spacing(5),
        },
        underHead: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            color: 'white',
        },
        whiteButton: {
                    color: 'white',
        }
    }),
);

const images = [
    'https://via.placeholder.com/600/fff.png/000/fff',
    'https://via.placeholder.com/700/fff.png/000/fff',
    'https://via.placeholder.com/800/fff.png/000/fff',
    'https://via.placeholder.com/900/fff.png/000/fff',
    'https://via.placeholder.com/1000/fff.png/000/fff',
];

const populates = [
    { collection: 'users', child: 'owner', root: 'users' } // replace owner with user object
];

const DetailPage = (id) => {

    useFirestoreConnect([
        { collection: 'products' }, // or 'todos'
        { collection: 'users' }
    ]);

    const detailData = useSelector<any>((state) => state.firestore.data.productDetail);

   console.log(detailData);

    const classes = useStyles();

    const dispatch = useDispatch();
    const params = useSelector(getid);
    const product = useSelector(getDetail);
    const [modalOpen, handleModal] = useState(false);
    const [imgIndex, handleImgindex] = useState<number | undefined >(undefined);

    const getCarouselData = () => images.map((e) => {
        return {source: e};
    });

    useEffect(() => {
        // dispatch(ResetDetail());
        dispatch(GetProductDetail(params));
    }, []);

    const showCarousel = (index) => {
        handleImgindex(index);
        handleModal(true);
    };

    const body = (product) => (
        <Container maxWidth="lg" className={classes.root}>

            <ModalGateway>
                {modalOpen ? (
                    <Modal onClose={() => handleModal(false)}>
                        <Carousel currentIndex={imgIndex} views={getCarouselData()}/>
                    </Modal>
                ) : null}

            </ModalGateway>

            <Typography align='left' variant='h4' gutterBottom={true}>{product.title}</Typography>

            <div className={classes.underHead}>
                <UserDisplay userName={"Martin Novák"}/>

                <div>
                    <Button className={classes.whiteButton} startIcon={<ShareIcon/>}>Share</Button>
                    <Button className={classes.whiteButton} startIcon={<FavoriteBorderIcon/>}>Save</Button>
                </div>
            </div>

            <ImagesGrid images={images} imageClicked={showCarousel}/>

            <Grid container>
                <Grid item md={9}>
                    <p>{product.body}</p>
                </Grid>

                <Grid item md={3}>
                    <Box className={classes.box}>
                        <BorrowForm price={100}/>
                        <Divider variant="middle" />
                        <Button variant='contained' color='secondary'>Půjčit</Button>
                    </Box>
                </Grid>
            </Grid>

        </Container>
    );

    return !!product ? body(product) : <div>nic</div>;
};

export default DetailPage;
