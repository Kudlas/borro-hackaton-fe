import React, {useEffect, useRef, useState} from 'react';
import ImagesGrid from "../../detail/ImagesGrid";
import {Button, FormHelperText, TextField} from "@material-ui/core";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {getNPStepData} from "../../../selectors";
import {submitStep} from "../../../actions/new";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        textarea: {
            width: '100%',
        },
        input: {
            visibility: 'hidden'
        },
        buttonLabel: {
            display: "flex",
            justifyContent: "center",
        },
        helperText: {
            textAlign: "center"
        }
    }),
);

const Np02 = ({setForm}) => {
    const classes = useStyles();
    const fileInput = useRef<HTMLInputElement | null>(null);
    const dispatch = useDispatch();
    const NPData = useSelector(getNPStepData);
    const [values, setValues] = useState(NPData[1]);

    const [images, setImages ] = useState<any[]>([]);
    const blankImage = 'https://static.thenounproject.com/png/187803-200.png';

    useEffect(() => {
        setTimeout(()=>{
            setForm( checkFormValidity );
        }, 200);

    }, [values]);

    const checkFormValidity = (): boolean => {

        // TODO check images uploaded

        dispatch(submitStep(1, values));
        return true;
    };

    const imageInputChanged = (e) => {
        console.log(e);
        const files = e.target.files;

        console.log(files);

        for (var i = 0; i < files.length; i++) {

            const file = files.item(i);

            let reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = (e) => {
                fillWithBlanks( [ ...(images.filter( (value) => value!==blankImage )) ,reader.result] );
            };
        }
    };

    const fillWithBlanks = (arr) => {
        const blanks = new Array(5 - arr.length).fill(blankImage);
        return [...arr, ...blanks];
    }

    return (
        <div>
            <label htmlFor="raised-button-file" className={classes.buttonLabel}>
                <Button variant="contained" color="secondary" component="span">
                    Nahraj fotky
                </Button>
            </label>
            <FormHelperText className={classes.helperText}>Nahraj alespoň dvě různé fotky tvého předmětu</FormHelperText>

            <ImagesGrid images={ fillWithBlanks(images) } imageClicked={() => {
                if(fileInput.current) fileInput.current.click();
            }}/>
            <TextField
                className={classes.textarea}
                label="Popis"
                variant="filled"
                helperText="Zde popiš předmět, pro případné zájemce"
                multiline
                rows={6}
                value={values.desc}
                onChange={(e) => setValues({desc: e.target.value})}
            />

            <input
                ref={fileInput}
                accept="image/*"
                className={classes.input}
                id="raised-button-file"
                multiple
                type="file"
                onChange={imageInputChanged}
            />
        </div>
    );
};

export default Np02;