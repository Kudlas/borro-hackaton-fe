import React from 'react';
import { Route, Switch } from 'react-router'
import DetailPage from "../product/pages/detail";
import ListPage from "../product/pages/list";
import HomePage from "../home/pages/home";
import {history} from "./history";
import {ConnectedRouter} from "connected-react-router";
import MyStuffPage from "../account/pages/myStuff";
import StuffDetailPage from "../account/pages/stuffDetail";

const routeList = [
    {url: "/", exact: true, component: (<HomePage/>)},
    {url: "/product/detail/:id", component: (<DetailPage/>)},
    {url: "/product/listing", component: (<ListPage/>)},
    {url: "/my-stuff", exact: true, component: (<MyStuffPage/>)},
    {url: "/my-stuff/detail/:id", component: (<StuffDetailPage/>)},
];

const Routes = () => {
    return (
        <ConnectedRouter history={history}>
            <Switch>
                {routeList.map((data, i) =>
                    <Route key={i} path={data.url} {...('exact' in data) ? {exact: true} : {}}>
                        {data.component}
                    </Route>
                )}
            </Switch>
        </ConnectedRouter>
    );
};
export default Routes;
// export default Routes;
