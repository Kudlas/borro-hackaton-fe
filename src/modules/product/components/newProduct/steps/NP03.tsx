import React, {useEffect, useState} from 'react';
import {FormControlLabel, Grid, Switch, TextField} from "@material-ui/core";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {getNPStepData} from "../../../selectors";
import {submitStep} from "../../../actions/new";
import {Autocomplete} from "@material-ui/lab";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        textarea: {
            width: '100%',
        },
    }),
);
// todo get from db
const top100Films = [
    { title: 'The Shawshank Redemption', year: 1994 },
    { title: 'The Godfather', year: 1972 },
    { title: 'The Godfather: Part II', year: 1974 },
    { title: 'The Dark Knight', year: 2008 },
    { title: '12 Angry Men', year: 1957 } ];

const Np03 = ({setForm}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const NPData = useSelector(getNPStepData);
    const [values, setValues] = useState(NPData[2]);

    useEffect(() => {
        setTimeout(()=>{
            setForm( checkFormValidity );
        }, 200);

    }, [values]);

    const checkFormValidity = (): boolean => {

        // TODO check images uploaded

        dispatch(submitStep(2, values));
        return true;
    };

    return (
        <div>
            <TextField
                className={classes.textarea}
                label="Tvůj popisek"
                placeholder='Např. najdeš to ve třetí skříni vpravo, evidenční číslo. Příp. půjčujte jen v případě...'
                variant="filled"
                helperText="Tento popis uvidí jen správci předmětu"
                multiline
                rows={6}
                value={values.privateDesc}
                onChange={(e) => setValues({privateDesc: e.target.value})}
            />

            {/*groups*/}
            <Autocomplete
                multiple
                options={top100Films}
                ChipProps={ {color: 'secondary'} }
                getOptionLabel={(option) => option.title}
                defaultValue={[top100Films[3]]}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        helperText="Členové vybraných skupin mohou předmět půjčovat."
                        variant="filled"
                        label="Do které skupiny patří?"
                        placeholder="Vyber skupinu/y"
                    />
                )}
            />

            <Grid container>
                <Grid item xs={6}>
                    <FormControlLabel
                        control={<Switch checked={values.public} onChange={(e) => setValues({public: e.target.value})}/>}
                        label="Je veřejně vidět"
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControlLabel
                        control={<Switch checked={values.borrowable} onChange={(e) => setValues({borrowable: e.target.value})}/>}
                        label="Lze žádat o zapůjčení"
                    />
                </Grid>
            </Grid>

        </div>
    );
};

export default Np03;