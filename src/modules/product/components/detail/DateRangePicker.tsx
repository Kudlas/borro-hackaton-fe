import React, { useState, useContext, useRef } from "react";
/*
import { DatePicker } from "material-ui-pickers";
import { MuiPickersContext } from "material-ui-pickers";
import withStyles from "@material-ui/core/styles/withStyles";
import { styles as dayStyles } from "material-ui-pickers/DatePicker/components/Day";
import clsx from "clsx";

function DateRangePicker({
                             pickerComponent = DatePicker,
                             classes,
                             value,
                             onChange,
                             labelFunc,
                             format,
                             emptyLabel,
                             autoOk,
                             onClose,
                             ...props
                         }) {
    const PickerComponent = pickerComponent || DatePicker;
    const [begin, setBegin] = useState(value[0]);
    const [end, setEnd] = useState(value[1]);
    const [hover, setHover] = useState(undefined);
    const picker = useRef<any>();
    const utils = useContext(MuiPickersContext);

    const min = Math.min(begin, end || hover);
    const max = Math.max(begin, end || hover);

    function renderDay(day, selectedDate, dayInCurrentMonth, dayComponent) {
        return React.cloneElement(dayComponent, {
            onClick: e => {
                e.stopPropagation();
                if (!begin) setBegin(day);
                else if (!end) {
                    setEnd(day);
                    if (autoOk) {
                        onChange([begin, day].sort());
                        if(picker && picker.current) {
                            picker.current.close();
                        }
                    }
                } else {
                    setBegin(day);
                    setEnd(undefined);
                }
            },
            onMouseEnter: e => setHover(day),
            className: clsx(classes.day, {
                [classes.hidden]: dayComponent.props.hidden,
                [classes.current]: dayComponent.props.current,
                [classes.isDisabled]: dayComponent.props.disabled,
                [classes.isSelected]: day >= min && day <= max,
                [classes.beginCap]: utils!.isSameDay(day, min),
                [classes.endCap]: utils!.isSameDay(day, max),
                [classes.singleCap]: utils!.isSameDay(min, max)
            })
        });
    }

    const formatDate = date => utils!.format(date, format || utils!.dateFormat);

    return (
        <PickerComponent
            {...props}
            value={begin}
            renderDay={renderDay}
            onClose={() => {
                //onChange([begin, end].sort());
                if (onClose) onClose();
            }}
            onChange={() => {onChange(...([begin, end].sort()));}}
            onClear={() => {
                setBegin(undefined);
                setEnd(undefined);
                setHover(undefined);
                //onChange([]);
            }}
            ref={picker}
            labelFunc={(date, invalid) =>
                labelFunc
                    ? labelFunc([begin, end].sort(), invalid)
                    : date && begin && end
                    ? `${formatDate(begin)} - ${formatDate(end)}`
                    : emptyLabel || ""
            }
        />
    );
}

export const styles = theme => {
    const base = dayStyles(theme);
    return {
        ...base,
        day: {
            ...base.day,
            margin: 0,
            width: "40px",
            borderRadius: "0",
            transition: "border-radius .2s"
        },
        beginCap: {
            borderRadius: "50% 0 0 50%"
        },
        endCap: {
            borderRadius: "0 50% 50% 0"
        },
        singleCap: {
            borderRadius: "50%"
        }
    };
};

export default withStyles(styles, { name: "DateRangePicker" })(DateRangePicker);
*/
