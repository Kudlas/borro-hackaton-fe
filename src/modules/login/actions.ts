import {Creds} from "./types";

export enum LoginActionTypes {
    GoogleLogin = '[Login] Google login attempt',
    FacebookLogin = '[Login] Facebook login attempt',

    Login = '[Login] login attempt',
    LoginSuccess = '[Login] get login success',
    LoginFail = '[Login] get login failure',

    SignUpModal = '[Login] signup modal',
}

export const SignUpModal = (openClose: boolean) => ({
    type: LoginActionTypes.SignUpModal,
    openClose
});

export const Login = (creds: Creds) => ({
    type: LoginActionTypes.Login,
    creds
});

export const LoginFail = (err) => ({
    type: LoginActionTypes.LoginFail,
    err
});

export const LoginSuccess = (payload: any) => ({
    type: LoginActionTypes.LoginSuccess,
    payload
});

export const GoogleLogin = () => ({
    type: LoginActionTypes.GoogleLogin,
});

export const FacebookLogin = () => ({
    type: LoginActionTypes.FacebookLogin,
});


