import React, {useState} from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import {useDispatch, useSelector} from "react-redux";
import {getNewProductModalState} from "../../selectors";
import * as newProductActions from "../../actions/new";
import Np01 from "./steps/NP01";
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {Grid} from "@material-ui/core";
import Np02 from "./steps/NP02";
import Np03 from "./steps/NP03";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            outline: 'none',
        },
        modal: {
            outline: 'none',
            border: 'none'
        },
        stepper: {
            paddingLeft: 0,
            paddingRight: 0
        },
        button: {
            marginRight: theme.spacing(1),
        },
        instructions: {
            marginTop: theme.spacing(1),
            marginBottom: theme.spacing(1),
        },
        heading: {
            marginTop: theme.spacing(1),
            marginBottom: theme.spacing(1),
            textAlign: "left"
        },
        paper: {
            outline: 'none',
            position: 'absolute',
            width: 400,
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },

    }),
);

function getSteps() {
    return ['To nejdůležitější', 'Obrázky', 'Práva'];
}

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

/*
*
nejlépe definovat co to je - základní info
 - co to je
 - kde to je
 - kategorie
 - cena?
fotky a media
 - nahrání fotek
 - vyplnění popisu
přístupnost
 - vlastní popisek
 - je to ve skupině?
 - je to vidět?
* */

export default function NewProductModal() {
    const steps = getSteps();
    const dispatch = useDispatch();
    const modalState = useSelector(getNewProductModalState);
    const classes = useStyles();

    const [activeStep, setActiveStep] = React.useState(0);
    const [forms, setForms] = useState<any>({});
    const [modalStyle] = React.useState(getModalStyle);

    const getStepContent = () => {
        switch (activeStep) {
            case 0:
                return <Np01 setForm={(form: any) => setForms({...forms, 0: form})}/>;
            case 1:
                return <Np02 setForm={(form: any) => setForms({...forms, 1: form})}/>;
            case 2:
                return <Np03 setForm={(form: any) => setForms({...forms, 2: form})}/>;
            default:
                return 'Pomoc, tady nevím co mám dělat';
        }
    }

    const handleClose = () => {
        dispatch(newProductActions.NewProductModal(false));
    };

    const handleNext = () => {

        // dynamický index - activeStep ?
        if (forms[activeStep]()) {
            // TODO check form valid
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
    const handleReset = () => {
        // TODO reset forms
        setActiveStep(0);
    };

    const body = (
        <div style={modalStyle} className={classes.paper}>
            <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
            >
                <Grid item xs={8}>
                    <Typography className={classes.heading} variant="h5"><LibraryAddIcon/> Předmět</Typography>
                </Grid>
                <Grid item xs={2} alignContent={'flex-end'}>
                    <Button onClick={handleClose}> <HighlightOffIcon color='secondary'/> </Button>
                </Grid>
            </Grid>


            <Stepper className={classes.stepper} activeStep={activeStep}>
                {steps.map((label, index) => {
                    const stepProps: { completed?: boolean } = {};
                    const labelProps: { optional?: React.ReactNode } = {};

                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
            <div>
                {activeStep === steps.length ? (
                    <div>
                        <Typography className={classes.instructions}>
                            All steps completed - you&apos;re finished
                        </Typography>
                        <Button onClick={handleReset} className={classes.button}>
                            Reset
                        </Button>
                    </div>
                ) : (
                    <div>
                        {getStepContent()}
                        <div>
                            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                                Back
                            </Button>

                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleNext}
                                className={classes.button}
                            >
                                {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                            </Button>
                        </div>
                    </div>
                )}
            </div>
        </div>);

    return (
        <div className={classes.root}>
            <Modal open={modalState}
                   onClose={handleClose}
                   aria-labelledby="simple-modal-title"
                   aria-describedby="simple-modal-description"
            >
                {body}
            </Modal>
        </div>
    );
}
