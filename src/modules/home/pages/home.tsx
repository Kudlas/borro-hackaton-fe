import React, {useEffect} from 'react';
import ProductList from "../../product/components/ProductList";
import {showLoading} from "react-redux-loading-bar";
import {useDispatch} from "react-redux";
import {Container, createStyles, Grid, Paper, Theme} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {GetProducts} from "../../product/actions/listing";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            marginTop: 10
        },
        paper: {
            width: "100%",
            height: 200
        },
        control: {
            padding: theme.spacing(2),
        },
    }),
);

const HomePage = () => {
    const classes = useStyles();

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(GetProducts());
        dispatch(showLoading());
    });

    return (
        <Container maxWidth="lg">
            <Grid container className={classes.root} spacing={2} alignItems="center">
                <Grid container justify="center" spacing={2}>
                    <Grid xs={2} item>
                        <Paper className={classes.paper}/>
                    </Grid>
                    <Grid xs={10} item>
                        <ProductList/>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    );
};

export default HomePage;
