import {combineReducers } from 'redux';
import {firebaseReducer} from "react-redux-firebase";
import {firestoreReducer} from "redux-firestore";
import { loadingBarReducer } from 'react-redux-loading-bar';

import ProductsReducer from "./Products";
import LoginReducer from "./Login";
import ModalReducer from "./Modals";

import {connectRouter} from "connected-react-router";

const RootReducer = (history) => combineReducers({
    router: connectRouter(history),
    loadingBar: loadingBarReducer,
    products: ProductsReducer,
    firebase: firebaseReducer,
    firestore: firestoreReducer,
    login: LoginReducer,
    modals: ModalReducer
});

export default RootReducer;
