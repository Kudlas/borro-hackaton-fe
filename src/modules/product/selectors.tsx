export const getProductsSelector = state => state.products.products;
export const showLoading = state => state.products.loadingProducts;

export const getDetail = state => state.products.detail;

export const getNewProductModalState = state => state.modals.newProductModal;

export const getNPStepData = state => state.products.newProduct;