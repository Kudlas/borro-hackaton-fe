import React from 'react';
import {makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import GridImage from "./GridImage";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            height: 250,
            marginBottom: theme.spacing(2)
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,

        },
        fullHeight: {
            height: "100%"
        },
        halfHeight: {
            height: "50%"
        }
    }),
);

interface iImagesGrid {
    images: string[],
    imageClicked: any,
}

const ImagesGrid = (props: iImagesGrid) => {
    const classes = useStyles();
    const spacing = 0;

    return (
        <div className={classes.root}>
                <Grid className={classes.fullHeight} container spacing={spacing} direction="row"
                      justify="center"
                      alignItems="stretch">
                    <Grid item xs={6} onClick={() => props.imageClicked(0)}>
                        <GridImage url={props.images[0]} title={'teehee'}  />
                    </Grid>

                    <Grid item xs={6} className={classes.fullHeight}>
                        <Grid container className={classes.halfHeight} item xs={12}>
                            <Grid item xs={6}  onClick={() => props.imageClicked(1)}>
                               <GridImage url={props.images[1]} title={'teehee'}/>
                            </Grid>

                            <Grid item xs={6}  onClick={() => props.imageClicked(2)}>
                                <GridImage url={props.images[2]} title={'teehee'}/>
                            </Grid>
                        </Grid>

                        <Grid item className={classes.halfHeight} container xs={12}>
                            <Grid item xs={6}  onClick={() => props.imageClicked(3)}>
                                <GridImage url={props.images[3]} title={'teehee'}/>
                            </Grid>

                            <Grid item xs={6}  onClick={() => props.imageClicked(4)}>
                                <GridImage url={props.images[4]} title={'teehee'}/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
        </div>
    );
};

export default ImagesGrid;