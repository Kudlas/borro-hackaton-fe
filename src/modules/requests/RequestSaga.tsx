import {call, takeEvery, put} from "redux-saga/effects";
import {FormatPostData} from "./Requests";
import {hideLoading, showLoading} from "react-redux-loading-bar";
import {API_PATH} from "../../Config";

function* requestRequest(action) { // major major
    if (!("request" in action)) return;
    if (!("loading" in action)) yield put(showLoading());

    let data;
    try {
        data = yield call(postData, action.request);
    } catch (e) {
        if ("fail" in action)
            yield put((action.fail)(data));
        return;
    } finally {
        yield put(hideLoading());
    }

    if ("success" in action)
        yield put((action.success)(data));
}

function* requestSaga() {
    yield takeEvery('*', requestRequest);
}

const postData = async (request) => {
    const postData:object | undefined = request.data ? FormatPostData(request.data) : undefined;

    return await fetch(`${API_PATH}${request.url}`, postData)
        .then(response => (
                (response.status === 200) ? response.json() : Promise.reject('timeout')
            )
        );
}

export default requestSaga;