import React from 'react';

import {useDispatch, useSelector} from "react-redux";
import {createStyles, Theme} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

import ProductCard from "./card";
import {getProductsSelector} from "../selectors";
import {push} from "connected-react-router";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: '100%',
            flexWrap: 'wrap'
        },
        item: {
            position: 'relative',
            flexGrow: 1,
            width: '25%',
            margin: '8px',
            height: 350
        }
    }),
);


export default function ProductList() {
    const classes = useStyles();
    const products = useSelector(getProductsSelector);

    const dispatch = useDispatch();

    const cardClicked = (id) => {
        dispatch(push('/product/detail/'+id))
    }

    return (!!products.length) ?
        (<div className={classes.root}> {products.map((i) => {
            return <div onClick={() => cardClicked(i.id)} className={classes.item} key={i.id}><ProductCard id={i.id} title={i.title} body={i.body} ></ProductCard></div>
        })} </div>) : (<div>nou data</div>);
}
