import React from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {ButtonBase, Typography} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        image: {
            backgroundSize: 'cover',
            backgroundPosition: 'center center',
            width: "100%",
            height: "100%",
            zIndex: 6,
            position: 'relative',
            '&:hover, &$focusVisible': {
                zIndex: 1,
                '& $imageBackdrop': {
                    opacity: 0.15,
                },
                '& $imageMarked': {
                    opacity: 0,
                },
                '& $imageTitle': {
                    border: '4px solid currentColor',
                },
            },
        },
        focusVisible: {},
    }),
);

interface iGridImage {
    url: string,
    title: string,
    width?: string,
}

const GridImage = (image: iGridImage) => {
    const classes = useStyles();

    return (
        <ButtonBase
            focusRipple
            className={classes.image}
            focusVisibleClassName={classes.focusVisible}
            style={{
                backgroundImage: `url(${image.url})`,
            }}
        >
        </ButtonBase>
    );
};

export default GridImage;