import React from 'react';
import {Avatar, Box} from "@material-ui/core";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        small: {
            width: theme.spacing(5),
            height: theme.spacing(5),
        },
        cont: {
            display:'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: 150
        }
    }),
);

interface iUserDisplay {
    userName: string,
    url?: string
}

const UserDisplay = (props: iUserDisplay) => {

    const classes = useStyles();

    const getInicials = (str) => {
            const res = str.split(" ");

            let result = ""
            res.map((a) => {
                result += a.substring(0,1);
            });

            return result;
    };

    const avatar = (props.url !== undefined) ?
        <Avatar alt={props.userName} src={props.url} /> :
        <Avatar className={classes.small}>{getInicials(props.userName)}</Avatar>;

    return (
        <Box m={2} className={classes.cont}>
            {avatar}
            <div>{props.userName}</div>
        </Box>
    );
};

export default UserDisplay;