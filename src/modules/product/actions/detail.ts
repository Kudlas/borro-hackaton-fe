export enum ProductDetailActionTypes {
    GetProductDetail = '[Products] get product detail',
    GetProductDetailSuccess = '[Products] get product detail success',
    GetProductDetailFail = '[Products] get product detail failure',

    ResetDetail = '[Products] product detail reset'
}

export const GetProductDetail = (id) => ({
    type: ProductDetailActionTypes.GetProductDetail,
    request: {
        type: "post",
        url: '/posts/'+id,
    },
    success: GetProductDetailSuccess,
    fail: GetProductDetailFail,
});

export const GetProductDetailSuccess = (payload: any) => ({
    type: ProductDetailActionTypes.GetProductDetailSuccess,
    payload
});

export const GetProductDetailFail = (payload: any) => ({
    type: ProductDetailActionTypes.GetProductDetailFail,
    payload
});

export const ResetDetail = () => ({
    type: ProductDetailActionTypes.ResetDetail,
});

