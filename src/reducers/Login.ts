import {LoginActionTypes} from "../modules/login/actions";

const initialState = {
    signupModal: false
};

const Login = (state = initialState, action: any) => {
  switch (action.type) {

      case LoginActionTypes.SignUpModal:
          return {
              ...state,
              signupModal: action.openClose
          };

      default:
          return state;
  }
};

export default Login;