import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createMuiTheme, ThemeProvider} from "@material-ui/core";
import {Provider} from "react-redux";
import store from "./store";
import {ReactReduxFirebaseProvider} from "react-redux-firebase";
import firebase from "firebase";
import {fbConfig, rrfConfig} from "./Config";
import { createFirestoreInstance, firestoreReducer } from 'redux-firestore' // <- needed if using firestore

firebase.initializeApp(fbConfig);
firebase.firestore();

const secondary = '#407E91';
const primary = '#916840';

const rrfProps = {
    firebase,
    config: rrfConfig,
    dispatch: store.dispatch,
    createFirestoreInstance // <- needed if using firestore
};

const customTheme = createMuiTheme({
    palette: {
        primary: {
            main: primary
        },
        secondary: {
            main: secondary,
        },
    },
});


ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
                <ReactReduxFirebaseProvider {...rrfProps}>
                    <ThemeProvider theme={customTheme}>
                        <App/>
                    </ThemeProvider>
                </ReactReduxFirebaseProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
