import {postHeaders, postHeadersAuth} from "../../Config";

export const FormatPostData = (data, isAuth?: boolean) => {

    const authHeaders: boolean = isAuth || false;

    const returnData = Object.keys(data).map((key) => {
        if(typeof data[key] !== "undefined")
        return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');

    return Object.assign({}, authHeaders ? postHeadersAuth : postHeaders, {body: returnData});
};

export const FormatGetData = (data, order?: string[]) => {

    const dataArray = order || Object.keys(data);

    const ret = dataArray.map( (key) => {
        return encodeURIComponent( data[key] );
    }).join('/');

    return ret;
};