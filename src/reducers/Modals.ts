import { ProductNewActionTypes} from "../modules/product/actions/new";

const initialState = {
    newProductModal: false
};

const ModalReducer = (state = initialState, action: any) => {
  switch (action.type) {
      case ProductNewActionTypes.ModalOpenage:
          return {
              ...state,
              newProductModal: action.openage
          };

      default:
          return state;
  }
};

export default ModalReducer;