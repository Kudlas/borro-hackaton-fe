export enum ProductNewActionTypes {
    ModalOpenage = '[Products] new product modal open',

    submitStep = '[Products] new product step submit'
}

export const NewProductModal = (openage: boolean) => ({
    type: ProductNewActionTypes.ModalOpenage,
    openage
});

export const submitStep = (stepNum: number, data: any) => ({
    type: ProductNewActionTypes.submitStep,
    stepNum,
    data
});