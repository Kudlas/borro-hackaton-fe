import React, { useRef, useState, useEffect } from "react";
import MaterialTable from "material-table";
import {
    Avatar,
    Chip,
    Container,
    FormControlLabel,
    Switch
} from "@material-ui/core";
import { MaterialTableProps } from "material-table";

const StuffTable = ({ rows, changeRows }) => {
    const tableRef = useRef<MaterialTableProps<object> | null>(null);
    const [newData, setNewData] = useState([]);
    const [counter, setCounter] = useState(0);

    useEffect(() => {
        if (newData.length) {
            changeRows(newData);
            //tableRef.current.onQueryChange(null);
        }
    }, [counter]);

    const changeRow = (oldRow, e) => {
        const { name, checked } = e.target;

        const changeData = { [name]: checked };
        const newRow = { ...oldRow, ...changeData };

        const index = rows.findIndex(dtaRow => dtaRow.id === oldRow.id);
        const newData = rows;
        newData[index] = newRow;

        setCounter(counter + 1);
        setNewData(newData);
    };

    return (
        <Container maxWidth="lg">
            <MaterialTable
                tableRef={tableRef}
                options={{
                    actionsColumnIndex: -1,
                    search: true,
                    exportButton: true,
                    exportDelimiter: ";"
                }}
                actions={[
                    {
                        icon: "edit",
                        tooltip: "Edit Study",
                        onClick: (event, rowData) => alert("Do you want to edit?")
                    }
                ]}
                columns={[
                    { title: "Název", field: "title" },
                    {
                        title: "Stav",
                        field: "status",
                        render: data => (
                            <Chip
                                label={data.status}
                                color="primary"
                                avatar={<Avatar src="/static/images/avatar/1.jpg" />}
                            />
                        )
                    },
                    {
                        title: "Půjčovat",
                        field: "borrowable",
                        render: (data, id) => (
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.borrowable}
                                        onChange={e => changeRow(data, e)}
                                        name="borrowable"
                                        color="primary"
                                    />
                                }
                                label={data.borrowable ? "půjčovat" : "nepůjčovat"}
                            />
                        )
                    },
                    {
                        title: "Vidí všichni",
                        field: "active",
                        render: (data: any) => (
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.visible}
                                        onChange={e => changeRow(data, e)}
                                        name="visible"
                                        color="primary"
                                    />
                                }
                                label={data.borrowable ? "půjčovat" : "nepůjčovat"}
                            />
                        )
                    },
                    { title: "Uskladněno", field: "location" }
                ]}
                data={rows}
                //data={query => changeRow(query)}
                title="Moje věci"
            />
        </Container>
    );
};

export default StuffTable;
