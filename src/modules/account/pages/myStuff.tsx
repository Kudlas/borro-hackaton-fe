import React from 'react';
import StuffTable from "../components/stuffTable";
import {useState} from "react";

let rows = [
    {id:5, title: "prošívanice", borrowable: false, surname: "Baran", status: "zapůjčeno", location: "Praha" },
    {id:6, title: "prošívanice 2", borrowable: false, surname: "Baran", status: "zapůjčeno", location: "Praha" },
    {id:7, title: "prošívanice 3", borrowable: false, surname: "Baran", status: "zapůjčeno" , location: "Brno"}
];


const MyStuffPage = () => {

    const [data, setData] = useState(rows);

    return (
        <div>
            <StuffTable rows={data} changeRows={(data) => {setData(data); console.log("hou",data);}} />
        </div>
    );
};

export default MyStuffPage;