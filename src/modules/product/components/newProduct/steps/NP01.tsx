import React, {MutableRefObject, useEffect, useRef, useState} from 'react';
import {
    Box, Button,
    FormControl,
    FormControlLabel, InputAdornment,
    InputLabel,
    MenuItem,
    Select,
    Switch,
    TextField
} from "@material-ui/core";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {submitStep} from "../../../actions/new";
import {getNPStepData} from "../../../selectors";
import PlaceField from "../fields/PlaceField";

const areas = [
    {id: 1, title: 'Praha'},
    {id:2, title: 'Brno'}
];
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            minWidth: 120,
            width: "50%"
        },
        payment: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'

        },
        nameField: {
            marginBottom: theme.spacing(2)
        },
        form: {
            display:'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            alignContent: 'stretch',
            marginBottom: theme.spacing(2)
        },
        money: {
            marginTop: theme.spacing(2.5)
        }
    })
);

const Np01 = ({setForm}) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const NPData = useSelector(getNPStepData);

    const [values, setValues] = useState(NPData[0]);
    const moneyFieldVisible = values.paymentWanted && values.payment===1;

    const [errors, setErrors] = useState<any>({
        area: false,
        payment: false,
        category: false,
        title: false,
        rewardAmount: false
    });

    useEffect(() => {
        setTimeout(()=>{
            setForm( checkFormValidity );
        }, 200);

        }, [values]);

    const checkFormValidity = (): boolean => {

        const keys = Object.keys(values);
        let errors = {};
        let ret = true;
        Object.values(values).map( (o, i) => {
            if(keys[i]==='paymentWanted') return null;
            if(keys[i]==='rewardAmount' && !moneyFieldVisible) return null;
            if(keys[i]==='payment' && !values.paymentWanted) return null;
            errors[keys[i]] = !o;
            ret = ret ? !!o : false;
            // console.log("field: ",keys[i], " variable ret: ",ret, ' errors array:',errors[keys[i]]);
        } );

        setErrors(errors);
           // console.log("is valid", ret);

        if (ret) {
            // dispatch step 01
            dispatch(submitStep(0, values));
        }

        return ret;
    };

    const fieldInput = (e, name, value?: any) => {
        let data: any = null;
        if(typeof value === "undefined") {
            data = {...values, [name]: e.target.value};
        }
        else
        {
            let val = (!!value) ? value : undefined;
            data = {...values, [name]: val};

        }

        setValues(data);
    };

    const getAvalue = (obj) => {
        return (!!obj && 'id' in obj) ? obj.id : undefined;
    };

    const formSubmit = (e) => {
        e.preventDefault();
    };

    const optionById = (id) => {
       const objs = areas.filter((obj) => obj.id===id);
       return objs[0];
    }

    return (
        <form autoComplete="off" className={classes.form} onSubmit={formSubmit}>
            <TextField
                error={!!errors['title']}
                required
                label="Co to je?"
                variant="filled"
                helperText="Stačí krátký popis"
                onChange={(e) => fieldInput(e, 'title')}
                value={values.title}
            />
            <PlaceField
                url='http://localhost:8090/api/get-places'
                inputProps={
                    {
                        label:"Kde to lze vyzvednout?",
                        helperText:"Psaním do políčka lze hledat v možnostech",
                        error: !!errors['area'],
                        variant: 'filled',
                    }
                }
                selectProps={{
                    value: values.area,
                    onChange: (e,v) => {
                        setValues({...values, 'area': v});
                    }
                }}
            />

            <PlaceField
                url='http://localhost:8090/api/get-cats'
                inputProps={
                    {
                        label:"Kategorie",
                        helperText:"Aby se dala věc lépe najít...",
                        error: !!errors['category'],
                        variant: 'filled'
                    }
                }
                selectProps={{
                    value: (values.category) ? values.category : [],
                    multiple: true,
                    onChange: (e,v) => {
                        setValues({...values, 'category': v});
                    }
                }}
            />

            <Box className={classes.payment}>
                <FormControlLabel
                    control={<Switch checked={values.paymentWanted} onChange={(e) => {
                        fieldInput(e,'paymentWanted',e.target.checked)
                    }}/>}
                    label="Chci odměnu"
                />

                <FormControl variant="filled" className={classes.formControl} disabled={!values.paymentWanted}>
                    <InputLabel id="paymentlabel">Co za to?</InputLabel>
                    <Select
                        error={!!errors['payment']}
                        required
                        labelId="paymentlabel"
                        value={values.payment}
                        // onChange={paymentChange}
                        onChange={(e) => fieldInput(e, 'payment')}
                    >
                        <MenuItem value={1}>Peníze</MenuItem>
                        <MenuItem value={2}>Alkohol</MenuItem>
                        <MenuItem value={3}>Čokoládu</MenuItem>
                    </Select>
                </FormControl>
            </Box>

            <TextField
                error={!!errors['rewardAmount']}
                className={classes.money}
                style={{visibility: (moneyFieldVisible) ? 'visible' : 'hidden' }}
                required={moneyFieldVisible}
                value={values.rewardAmount}
                label="Kolik"
                variant="filled"
                onChange={(e) => fieldInput(e, 'rewardAmount')}
                InputProps={{
                    endAdornment: <InputAdornment position="start">Kč / den</InputAdornment>,
                }}
            />

        </form>
    );
};

export default Np01;
