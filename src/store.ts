import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga';
import {composeWithDevTools} from "redux-devtools-extension";

import RootReducer from "./reducers";
import ProductsSaga from "./modules/product/sagas";
import LoginSaga from "./modules/login/sagas";
import {routerMiddleware} from "connected-react-router";

import {history} from './modules/router/history';
import requestSaga from "./modules/requests/RequestSaga";

const ProductSagaMiddleware = createSagaMiddleware();
const LoginSagaMiddleware = createSagaMiddleware();
const RequestSagaMiddleware = createSagaMiddleware();

const store = createStore(
    RootReducer(history),
    composeWithDevTools(
        applyMiddleware(routerMiddleware(history)),
        applyMiddleware(ProductSagaMiddleware),
        applyMiddleware(LoginSagaMiddleware),
        applyMiddleware(RequestSagaMiddleware),
    )
);

ProductSagaMiddleware.run(ProductsSaga);
ProductSagaMiddleware.run(LoginSaga);
ProductSagaMiddleware.run(requestSaga);

const action = type => store.dispatch({type});

export default store;
