import React from 'react';
import {CircularProgress, TextField} from "@material-ui/core";
import {Autocomplete} from "@material-ui/lab";

interface Data {
    id: number;
    name: string;
}


const PlaceField = ({url, inputProps, selectProps}) => {

    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState<Data[]>([]);
    const loading = open && options.length === 0;

    React.useEffect(() => {
        let active = true;

        if (!loading) {
            return undefined;
        }

        (async () => {
            const response = await fetch(url);
            const places = await response.json();

            if (active) {
                let dataFormatted = Object.keys(places).map((key) => { return {id: parseInt(key), name: String(places[key])}; }) as Data[];
                setOptions( dataFormatted );
            }

        })();

        return () => {
            active = false;
        };
    }, [loading]);

    React.useEffect(() => {
        if (!open) {
            // setOptions([]);
        }
    }, [open]);

    return (
        <Autocomplete
            open={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            {...selectProps}
            getOptionSelected={(option: Data, value) => option.name === value.name}
            getOptionLabel={(option: Data) => option.name}
            options={options}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    {...inputProps}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
};

export default PlaceField;
