import {call, put, fork, all, takeLatest } from 'redux-saga/effects'

import {GetProductsFail, GetProductsSuccess, ProductActionTypes} from "./actions/listing";
import {ProductsService} from "./services";
import {hideLoading} from "react-redux-loading-bar";
import {ProductNewActionTypes} from "./actions/new";

function* getProducts() {

    try {
        const data = yield call(ProductsService.getProducts);
        yield all([
            put(GetProductsSuccess(data)),
            put(hideLoading())
        ])
    } catch(e) {
        yield put( GetProductsFail() );
        return;
    }
}

function* validateStep(action) {

    console.log("kontrola jestli hodnoty existují");
    console.log(action);

}

function* ProductsSaga() {
    yield takeLatest(ProductActionTypes.GetProducts, getProducts);
    yield takeLatest(ProductNewActionTypes.submitStep, validateStep);
}

export default ProductsSaga;
