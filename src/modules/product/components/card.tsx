import React from 'react';
import {
    Card,
    CardActionArea,
    CardContent,
    CardMedia, Chip,
    Typography
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {trunc} from "../../utils/Utils";
import PinDropIcon from '@material-ui/icons/PinDrop';

interface ProductDataLight {
    id: number,
    title: string,
    body: string
}

const useStyles = makeStyles({
    root: {
        // maxWidth: 345,
        height: '100%'
    },
    media: {
        height: 220,
    },
    area: {
        height: 60
    },
    price: {
        marginLeft: 16
    },
    location: {
        opacity: .85,
        zIndex: 5,
        position: 'absolute',
        top: 5,
        right: 5
    }
});

// https://material-ui.com/components/cards/
export default function ProductCard(data: ProductDataLight) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <Chip
                label={"Praha - Vinohrady"}
                icon={<PinDropIcon />}
                className={classes.location}
                color="secondary"
            />
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image="https://miro.medium.com/max/1400/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg"
                    title="Contemplative Reptile"
                />
                <CardContent className={classes.area}>
                    <Typography align='left' gutterBottom variant="h6" component="h2">
                        {trunc(data.title, 45)}
                    </Typography>
                </CardContent>
            </CardActionArea>

            <div>
                <Typography color="textSecondary" align='left' gutterBottom className={classes.price}>
                    Zdarma
                </Typography>
            </div>


        </Card>
    );
}