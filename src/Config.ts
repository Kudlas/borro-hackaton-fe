
export const API_PATH = 'https://jsonplaceholder.typicode.com';

// react-redux-firebase config
export const rrfConfig = {
    userProfile: 'users',
    useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
}

export const fbConfig = {
    apiKey: "AIzaSyBtxDIUwFdzu-XyINCspqKP2stgkF8MfAM",
    authDomain: "borro-467eb.firebaseapp.com",
    databaseURL: "https://borro-467eb.firebaseio.com",
    projectId: "borro-467eb",
    storageBucket: "borro-467eb.appspot.com",
    messagingSenderId: "241408776717",
    appId: "1:241408776717:web:f21414697381ce8a151344",
    measurementId: "G-NPQ0GB66LT"
};

export const postHeaders = {
    method: 'POST',
    mode: 'cors',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
};

export const postHeadersAuth = (() => {
    return {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': "Bearer " + localStorage.getItem('jwt')
        }
    }
})();