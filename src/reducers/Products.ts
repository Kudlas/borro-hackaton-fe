import {ProductActionTypes} from "../modules/product/actions/listing";
import {ProductDetailActionTypes} from "../modules/product/actions/detail";
import {ProductNewActionTypes} from "../modules/product/actions/new";

const initialState = {
    products: [],
    loadingProducts: false,
    detail: null,
    newProduct: {
        0: {
            title: '',
            area: '',
            category: '',
            payment: 0,
            paymentWanted: true,
            rewardAmount: '',
        },
        1: {
            desc: ''
        },
        2: {
            privateDesc: '',
            groups: [],
            public:true,
            borrowable: true
        }
    },
};

const Products = (state = initialState, action: any) => {
  switch (action.type) {
      case ProductActionTypes.GetProductsSuccess:
          return {
              ...state,
              products: action.payload,
              loadingProducts: false
          };
      case ProductActionTypes.GetProducts:
          return {
              ...state,
              loadingProducts: true
          };
      case ProductDetailActionTypes.ResetDetail:
          return {
              ...state,
              detail: null
          };
      case ProductDetailActionTypes.GetProductDetailSuccess:
          return {
              ...state,
              detail: action.payload
          };

      case ProductNewActionTypes.submitStep:
          return {
              ...state,
              newProduct: {
                  ...state.newProduct,
                  [action.stepNum]: action.data,
              }
          };

      default:
          return state;
  }
};

export default Products;
