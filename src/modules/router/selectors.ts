export const url = state => state.router.location.pathname.split('/');
export const getid = state => url(state).slice(-1)[0];
